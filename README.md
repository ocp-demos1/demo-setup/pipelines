# Samples for creating a pipeline from CLI


# Steps

1. Ensure you are logged in as an admin on cluster - `oc whoami`

2. `subscribe` to the Red Hat OpenShift Pipelines Operator from the UI or CLI. 

CLI command: `oc apply -f https://gitlab.com/ocp-demos1/demo-setup/pipelines/-/raw/main/install-operator.yaml` 

3. 